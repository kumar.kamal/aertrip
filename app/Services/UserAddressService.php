<?php 

namespace App\Services;

use App\Models\UserAddress;
use Exception;

class UserAddressService
{
	/**
     * Store a newly created resource in storage.
     *
     * @param array $request
 	*/
	public function save($request){
		try {
		    $address = new UserAddress();
		    $address->address = $request['address'];
		    $address->user_id = $request['user_id'];
	     	$address->save();
	     	return $address;
		} catch (\Throwable $th) {
		   throw new \Exception($th);
		}
	}

	/**
     * Update the specified resource in storage.
     *
     * @param array $request
     * @param int $id
 	*/
	public function update($request, $id){
		try {
		    $address = UserAddress::find($id);
		   	$address->update($request);
	     	return  $address;
		}  catch (\Throwable $th) {
		     throw new \Exception($th);
		}
	}

	/**
     * Display a specified resource from storage.
     *
     * @param int $id
 	*/
	public function getOne($id){
		try {
		    return UserAddress::with(['user'])->find($id);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param int $id
 	*/

	public function delete($id)
	{
		try {
		    return UserAddress::destroy($id);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}

	/**
     * Display a listing of the resource.
     *
     * @param int $limit
     * @param string $search
     * @param int $userId
 	*/
	public function getList($limit, $search, $userId){
		try {
			$contact = UserAddress::with(['user'])->whereUserId($userId);
			if(!empty($search)){
				$contact = $contact->where('address', "LIKE", '%'.$search.'%');
			}
			return $contact->paginate($limit);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}
}