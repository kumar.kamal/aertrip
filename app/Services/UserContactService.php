<?php 

namespace App\Services;

use App\Models\UserContact;
use Exception;

class UserContactService
{
	/**
     * Store a newly created resource in storage.
     *
     * @param array $request
 	*/
	public function save($request){
		try {
		    $contact = new UserContact();
		    $contact->contact_no = $request['contact_no'];
		    $contact->user_id    = $request['user_id'];
	     	$contact->save();
	     	return  $contact;
		} catch (\Throwable $th) {
		   throw new \Exception($th);
		}
	}

	/**
     * Update the specified resource in storage.
     *
     * @param array $request
     * @param int $id
 	*/
	public function update($request, $id){
		try {
		    $contact = UserContact::find($id);
		   	$contact->update($request);
	     	return  $contact;
		}  catch (\Throwable $th) {
		     throw new \Exception($th);
		}
	}

	/**
     * Display a specified resource from storage.
     *
     * @param int $id
 	*/
	public function getOne($id){
		try {
		    return UserContact::with(['user'])->find($id);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param int $id
 	*/

	public function delete($id)
	{
		try {
		    return UserContact::destroy($id);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}

	/**
     * Display a listing of the resource.
     *
     * @param int $limit
     * @param string $search
     * @param int $userId
 	*/
	public function getList($limit, $search, $userId){
		try {
			$contact = UserContact::with(['user'])->whereUserId($userId);
			if(!empty($search)){
				$contact = $contact->where('contact_no', "LIKE", '%'.$search.'%');
			}
			return $contact->paginate($limit);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}
}