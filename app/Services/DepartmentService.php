<?php 

namespace App\Services;

use App\Models\Department;
use Exception;

class DepartmentService
{
	/**
     * Store a newly created resource in storage.
     *
     * @param array $request
 	*/
	public function save($request){
		try {
		    $department = new Department();
		    $department->name    = $request['name'];
		    $department->comment = $request['comment'];
	     	$department->save();
	     	return  $department;
		} catch (\Throwable $th) {
		   throw new \Exception($th);
		}
	}

	/**
     * Update the specified resource in storage.
     *
     * @param array $request
     * @param int $id
 	*/
	public function update($request, $id){
		try {
		    $department = Department::find($id);
		   	$department->update($request);
	     	return  $department;
		}  catch (\Throwable $th) {
		     throw new \Exception($th);
		}
	}

	/**
     * Display a specified resource from storage.
     *
     * @param int $id
 	*/
	public function getOne($id){
		try {
		    return Department::find($id);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param int $id
 	*/

	public function delete($id){
		try {
		    return Department::destroy($id);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}

	/**
     * Display a listing of the resource.
     *
     * @param int $limit
     * @param string $search
 	*/
	public function getList($limit, $search){
		try {
			if(!empty($search)){
				return Department::where('name', "LIKE", '%'.$search.'%')->paginate($limit);
			} else {
				return Department::paginate($limit);  
			}
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}
}