<?php 

namespace App\Services;

use App\Models\User;
use Exception;

class UserService
{
	/**
     * Store a newly created resource in storage.
     *
     * @param array $request
 	*/
	public function save($request){
		try {
		    $user = new User();
		    $user->name  = $request['name'];
		    $user->email    = $request['email'];
		    $user->password = \Hash::make($request['password']);
		    $user->department_id = $request['department_id'];
	     	$user->save();
	     	return  $user;
		} catch (\Throwable $th) {
		   throw new \Exception($th);
		}
	}

	/**
     * Update the specified resource in storage.
     *
     * @param array $request
     * @param int $id
 	*/
	public function update($request, $id){
		try {
		    $user = User::find($id);
		    if(!empty($request['password'])){
		    	$request['password'] = \Hash::make($request['password']);
		    }
		   	$user->update($request);
	     	return  $user;
		}  catch (\Throwable $th) {
		     throw new \Exception($th);
		}
	}

	/**
     * Display a specified resource from storage.
     *
     * @param int $id
 	*/
	public function getOne($id){
		try {
		    return User::with(['department'])->find($id);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param int $id
 	*/

	public function delete($id){
		try {
		    return User::destroy($id);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}

	/**
     * Display a listing of the resource.
     *
     * @param int $limit
     * @param string $search
 	*/
	public function getList($limit, $search){
		try {
			$user = User::with(['contacts', 'addresses', 'department']);
			if(!empty($search)){
				$user = $user->where('name', "LIKE", '%'.$search.'%')->where('email', "LIKE", '%'.$search.'%');
				$user = $user->orWhereHas('department', function($q) use($search){
				    $q->where('name', "LIKE", '%'.$search.'%');
				});
			} 
			return $user->paginate($limit);
		} catch (\Throwable $th) {
		    throw new \Exception($th);
		}
	}
}