<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Exception;
use App\Services\UserService;

class BaseController extends Controller
{
   /**
    * Success response method
    * 
    * @return \Illuminate\Http\Response
    */

    public function sendResponse($result=[], $message='')
    {
       $response=[
            'status'   => 'success',
            'code'     => 200,
            'message'  => $message,
            'data'     => $result
        ];
        return response()->json($response, 200);
    }

   /**
    * Error response method
    * 
    * @return \Illuminate\Http\Response
    */
    public function sendError($message, $validation=[], $code=406)
    {
       $response=[
            'status'     => 'fail',
            'code'       => $code,
            'message'    => $message,
            'validation' => $validation,
            'data'       => []
        ];
        return response()->json($response, $code);
    }

}
