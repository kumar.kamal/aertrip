<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Validator;
use Exception;
use App\Services\UserContactService;
use App\Models\UserContact;
use App\Models\User;


class UserContactController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $perPage   = !empty($request->per_page) ? $request->per_page : 10;
            $searchKey = !empty($request->search_key)  ? $request->search_key : '';
            $userId    = !empty($request->user_id)  ? $request->user_id : null;
            return $this->sendResponse(UserContactService::getList($perPage, $searchKey, $userId));
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'    => 'required|numeric|exists:'.(new User)->getTable().',id',
            'contact_no' => 'required|numeric|digits:10|unique:'.(new UserContact)->getTable().',contact_no,NULL,id,user_id,'.$request->user_id,
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        try {
            $contact = UserContactService::save($request->all());
            return $this->sendResponse($contact, 'Added successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->sendResponse(UserContactService::getOne($id));
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'contact_no' => 'required|numeric|digits:10|unique:'.(new UserContact)->getTable().',contact_no,'.$id.',id,user_id,'.$request->user_id,
            'user_id'    => 'required|numeric|exists:'.(new User)->getTable().',id',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        try {
            $contact = UserContactService::update($request->all(), $id);
            return $this->sendResponse($contact, 'Updated successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            UserContactService::delete($id);
            return $this->sendResponse([], 'Deleted successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }
}
