<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Validator;
use Exception;
use App\Services\DepartmentService;
use App\Models\Department;

class DepartmentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $perPage   = !empty($request->per_page) ? $request->per_page : 10;
            $searchKey = !empty($request->search_key)  ? $request->search_key : '';
            return $this->sendResponse(DepartmentService::getList($perPage, $searchKey));
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'    => 'required|string|max:100|unique:'.(new Department)->getTable().',name',
            'comment' => 'nullable|string|max:200'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        try {
            $department = DepartmentService::save($request->all());
            return $this->sendResponse($department, 'Added successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->sendResponse(DepartmentService::getOne($id));
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validator = Validator::make($request->all(), [
            'name'    => 'required|string|max:100|unique:'.(new Department)->getTable().',name,'.$id,
            'comment' => 'nullable|string|max:200',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        try {
            $department = DepartmentService::update($request->all(), $id);
            return $this->sendResponse($department, 'Updated successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DepartmentService::delete($id);
            return $this->sendResponse([], 'Deleted successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }
}
