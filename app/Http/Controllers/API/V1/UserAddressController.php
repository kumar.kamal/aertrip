<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Validator;
use Exception;
use App\Services\UserAddressService;
use App\Models\UserAddress;
use App\Models\User;


class UserAddressController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $perPage   = !empty($request->per_page) ? $request->per_page : 10;
            $searchKey = !empty($request->search_key)  ? $request->search_key : '';
            $userId    = !empty($request->user_id)  ? $request->user_id : null;
            return $this->sendResponse(UserAddressService::getList($perPage, $searchKey, $userId));
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric|exists:'.(new User)->getTable().',id',
            'address' => 'required|string|unique:'.(new UserAddress)->getTable().',address,null,id,user_id,'.$request->user_id.'|max:200',

        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        try {
            $address = UserAddressService::save($request->all());
            return $this->sendResponse($address, 'Added successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->sendResponse(UserAddressService::getOne($id));
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric|exists:'.(new User)->getTable().',id',
            'address' => 'required|string|unique:'.(new UserAddress)->getTable().',address,'.$id.',id,user_id,'.$request->user_id.'|max:200',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        try {
            $address = UserAddressService::update($request->all(), $id);
            return $this->sendResponse($address, 'Updated successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            UserAddressService::delete($id);
            return $this->sendResponse([], 'Deleted successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }
}
