<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Validator;
use Exception;
use App\Services\UserService;
use App\Models\User;
use App\Models\Department;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $perPage   = !empty($request->per_page) ? $request->per_page : 10;
            $searchKey = !empty($request->search_key)  ? $request->search_key : '';
            return $this->sendResponse(UserService::getList($perPage, $searchKey));
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required|string|max:100',
            'email'    => 'required|string|email|max:200|unique:'.(new User)->getTable().',email',
            'password' => 'required|min:6|max:15',
            'department_id' => 'required|numeric|exists:'.(new Department)->getTable().',id',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        try {
            $user = UserService::save($request->all());
            return $this->sendResponse($user, 'Added successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->sendResponse(UserService::getOne($id));
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required|string|max:100',
            'email'    => 'required|string|email|unique:'.(new User)->getTable().',email,'.$id,
            'password' => 'nullable|min:6|max:15',
            'department_id' => 'required|numeric|exists:'.(new Department)->getTable().',id',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $user = UserService::update($request->all(), $id);
            return $this->sendResponse($user, 'Updated successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            UserService::delete($id);
            return $this->sendResponse([], 'Deleted successfully');
        } catch (Exception $e) {
            return $this->sendError('Somethng went wrong', [], 500); 
        }
    }
}
