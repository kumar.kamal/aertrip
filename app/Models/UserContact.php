<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'contact_no'
    ];

    /**
     * Get the employee that owns the contact.
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
