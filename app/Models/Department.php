<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'comment'
    ];

    /**
     * @return HasMany
     * @description get the employees associated with the department
     */
    public function users()
    {
        return $this->hasMany(User::class, 'department_id');
    }
}
