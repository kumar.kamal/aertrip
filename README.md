## Aertrip 

## Project Setup Steps on Local



```
- Clone Project on your local system.
 	- Install git on your system.
 	- Run- git clone https://gitlab.com/kumar.kamal/aertrip.git 
- Rename .env.example as .env.
- Enter database connection details.
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=aertrip
    DB_USERNAME=root
    DB_PASSWORD=
- Run command 
   composer install

```

## Project Document 
- [ ] [Documentation URL](https://documenter.getpostman.com/view/22106394/2s93RTQY5y#b8db6791-1706-419b-a61f-8e953356cb4d)
